var express = require('express');
var  app = express();

app.get('/', function (req, res) {
  res.send('Hello! Don Gori');
});

app.listen(3000, function() {
  console.log('App listening on PORT 3000');
});

module.exports = app;
