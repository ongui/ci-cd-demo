var request = require('supertest'); 
var app = require('../server.js');

describe('GET /', function() {
  it('displays "Hello! Don Gori"', function(done)  {
    request(app).get('/').expect('Hello! Don Gori', done);
  });
});
